# LEIA-ME

Este plugin alterar a Produção (Individual) e Pedido de Industrialização para utilizar a Lista de Material do IDempiere Libero (Tabelas PP_Product_BOM e PP_Product_BOMLine) ao invés da Lista de Material padrão do IDempiere (Tabelas M_Product_BOM e M_Product_BOMLine). Também permite a crição de Lista de Material por Parceiro de Negócio

## COMO INSTALAR

Baixe o plugin no formato **JAR** e instale no seu ambiente. 

[Clique aqui para fazer o Download.](https://bitbucket.org/kenos/org.kenos.idempiere.lbr.productionbom/downloads/)

**Script**

Execute o Script 201807191800_BOMLiberoOnProductionSimple.sql para:
	
 - Adicionar o campo Parceiro de Negócio na Janela Lista de Materiais.
 - Adicionar ao Validador de Modelo o Validator org.kenos.idempiere.lbr.productionbom.validator.VLBRProductionBom

Veja mais detalhes sobre como instalar [clicando aqui.](http://wiki.idempiere.org/en/Developing_Plug-Ins_-_Get_your_Plug-In_running)

## COMO USAR

Com plugin ativo, você deixará de utilizar a Aba **Lista de Material** na janela produto e passará a utilizar a janela encontrada no menu  Gerenciamento de Fabricação -> Gerência de Engenharia -> Lista de Material & Fórmulas -> **Lista de Materíais & Fórmula**

Você poderá adicionar Lista de Materiais com base nas seguinte regras:
 - É imprescindível ter uma Lista de Material para o produto sem o preenchimento do campo Parceiro de Negócio. Esta será a lista padrão utilizada tanto na Produção (Individual) quanto no Pedido de Industrialização
 - Em relação a Pedido de Industrialização, você poderá criar uma Lista de Material específica para um determinado Parceiro de Negócio. Não havendo uma lista específica o sistema utilizará a lista padrão.
 - Você poderá determinar um período de vigência (Data de Início e Fim) para cada Lista de Material 

**Observação**

Para utilizar a Lista de Material cadastrada continua sendo obrigatório a marcação do campo **Lista de Materiais** existente na janela **Produto**

## COMO CONTRIBUIR

Faça um fork deste projeto e proponha que as mudanças desejadas sejam integradas neste projeto.

## FALE COM O DESENVOLVEDOR

Para entrar em contato com os desenvolvedores [deixe sua mensagem aqui.](https://talk.kenos.com.br/)